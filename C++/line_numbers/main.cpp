#include <iostream>
#include <fstream>

using namespace std;

int main()
{
    cout << "Input file: ";
    string luettava_tiedosto;
    cin >> luettava_tiedosto;

    cout << "Output file: ";
    string kirjoitus_tiedosto;
    cin >> kirjoitus_tiedosto;

    ifstream luettava_olio(luettava_tiedosto);
    if(not luettava_olio) {
        cout << "Error! The file " << luettava_tiedosto << " cannot be opened." << endl;
        return EXIT_FAILURE;
    }
    else {
        ofstream kirjoittava_olio(kirjoitus_tiedosto);
        int i = 0;
        string rivi;
        while(getline(luettava_olio, rivi)) {
            kirjoittava_olio << i+1 << " ";
            kirjoittava_olio << rivi << endl;
            i++;
        }

        kirjoittava_olio.close();
        luettava_olio.close();
    }

    return EXIT_SUCCESS;
}
