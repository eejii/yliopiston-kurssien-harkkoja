<?php
   // Tama funktiokutsu jokaiseen
   // istuntoon sisaltyvan php-sivun alkuun
   session_start();
   
   if (isset($_SESSION['viesti'])) echo '<p style="color:red">'.$_SESSION['viesti'].'</p>';

   if(isset($_SESSION['summa'])) {
      echo '<table>';

         echo "{$_SESSION['nimi1']} on siirt�nyt ";
         
         echo "{$_SESSION['summa']} euroa"; 
    
         echo " henkil�lle {$_SESSION['nimi2']}";

      echo '</table>';
   }
    
   // Lopetetaan sessio
   if (isset($_POST['palaa'])) {
      session_destroy();
      // ja palataan sivulle Ohjelmointi.php
      header('Location: Ohjelmointi.php');
   }
?>
<html>
	<head>
		<title> Tulos  </title>
	</head>
	<body>
		<form method="post" action="Ohjelmointi2.php">
         <input type="submit" name ="palaa" value="Palaa"/>
      </form>
	</body>
</html>