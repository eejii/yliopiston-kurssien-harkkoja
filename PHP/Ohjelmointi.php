<?php
// Tama funktiokutsu jokaiseen
// istuntoon sisaltyvan php-sivun alkuun
session_start();
    
// luodaan tietokantayhteys ja ilmoitetaan mahdollisesta virheestä
$y_tiedot = "dbname=ej425076 user=ej425076 password=dsC5ryMfsOGXcG6";

$yhteys = pg_connect($y_tiedot);

if (!$yhteys = pg_connect($y_tiedot))
   die("Tietokantayhteyden luominen epäonnistui.");
 
if (isset($_POST['Siirrä'])) {
   // suojataan merkkijonot ennen kyselyn suorittamista

   $_SESSION['summa']  = $_POST['summa'];
   $_SESSION['veloitettava']  = $_POST['veloitettava'];
   $_SESSION['siirrett�v�'] = $_POST['siirrettävä'];
   $_SESSION['nimi1'];

   $tiedot_ok = $_SESSION['summa'] != 0 && $_SESSION['veloitettava'] != 0 &&$_SESSION['siirrett�v�'] != 0;
   
   $taulu = 'tilit';
   
   if ($tiedot_ok){
      $tulos3 = pg_query("SELECT omistaja FROM Tilit WHERE tilinumero = {$_SESSION['veloitettava']}");
      if (!$tulos3) {
         echo "Virhe kyselyss�.\n";;
         exit;
      }
      while ($rivi = pg_fetch_row($tulos3)) {
         $_SESSION['nimi1'] = $rivi[0];
      }

      $tulos4 = pg_query("SELECT omistaja FROM Tilit WHERE tilinumero = {$_SESSION['siirrett�v�']}");
      if (!$tulos4) {
         echo "Virhe kyselyss�.\n";;
         exit;
      }
      while ($rivi = pg_fetch_row($tulos4)) {
         $_SESSION['nimi2'] = $rivi[0];
      }


      pg_query('BEGIN')
 		or die('Ei onnistuttu aloittamaan tapahtumaa:' . pg_last_error());

      $tulos = pg_query('UPDATE ' . $taulu.' 
                        SET summa = summa - ' . $_SESSION['summa'] .' 
                        WHERE tilinumero = \'' . $_SESSION['veloitettava'] . '\'' .' AND summa > ' . $_SESSION['summa'])
               or die('Virhe ensimm�isess� p�ivityksess�: ' . pg_last_error());

      if (pg_affected_rows($tulos) != 1){
         pg_query('ROLLBACK')
         or die('Ei onnistuttu perumaan tapahtumaa: ' . pg_last_error());
         return 'L�hdetilin tilinumero on v��r� tai saldoa ei ole tarpeeksi.';
      }
      $tulos1 = pg_query('UPDATE ' . $taulu.' 
                       SET summa = summa + ' . $_SESSION['summa'] .' 
                       WHERE tilinumero = \'' . $_SESSION['siirrett�v�'] . '\'')
                or die('Virhe toisessa p�ivityksess�: ' . pg_last_error()); 

      if (pg_affected_rows($tulos1) != 1){
         pg_query('ROLLBACK')
         or die('Ei onnistuttu perumaan tapahtumaa: ' . pg_last_error());
         return 'Vastaanottavan tilin tilinumero on v��r�.';
      }

      pg_query('COMMIT')
      or die('Ei onnistuttu hyv�ksym��n tapahtumaa: ' . pg_last_error());

   }
   // ja siirrytaan sivulle Ohjelmointi2.php
   header('Location: Ohjelmointi2.php');

}
 
// suljetaan tietokantayhteys

pg_close($yhteys);
?>

<html>
   <head>
      <title> Tilisiirto </title>
   </head>
      <body>
         <!-- Lomake lähetetään samalle sivulle (vrt lomakkeen kutsuminen) -->
         <form action="Ohjelmointi.php" method="post">
            <h2>Tilisiirto</h2>
      
            <?php if (isset($_SESSION['viesti'])) echo '<p style="color:red">'.$_SESSION['viesti'].'</p>'; ?>

            <table border="0" cellspacing="0" cellpadding="3">
               <tr>
                  <td> Veloitettava summa </td>
                  <td><input type="text" name= "summa" value="" /></td>
               </tr>
               <tr>
                  <td>Veloitettava tilinumero</td>
                  <td><input type="text" name="veloitettava" value="" /></td>
               </tr>
               <tr>
                  <td>Tilinumero jonne summa siirretään</td>
                  <td><input type="text" name="siirrettävä" value="" /></td>
               </tr>
            </table>
            <input type="hidden" name="Siirrä" value="jep" />
            <input type="submit" value="Siirrä" />
         </form>
      </body>
</html>