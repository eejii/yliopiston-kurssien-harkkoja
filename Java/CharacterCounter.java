/*
 * Viikkoharjoitus 5, tehtävä 6.
 *
 * Lausekielinen ohjelmointi II, syksy 2020.
 *
 * Ohjelma laskee montako esiintymää 1D taulukon alkioilla on 2D taulukossa.
 *
 * Esteri Jokinen (esteri.jokinen@tuni.fi).
 *
 */
 
public class CharacterCounter {
   /*
    * Metodi saa parametreina 1D taulukon ja 2D taulukon ja laskee
    * 1D taulukon alkioiden esiintymine lukumäärät 2D taulukossa ja 
    * sijoittaa ne taulukkoon. Paluuarvona on null mikäli parametreille
    * ei ole varattu muistia tai ne ovat tyhjiä, muuten palautetaan viite
    * luotuun taulukkoon.
    */
   public static int[] laskeFrekvenssit (char[] taulu1, char[][] taulu2) {
      // Tarkastetaan että taulukot eivät ole tyhjiä ja 
      // niille on varattu muistia.
      if(taulu1 != null && taulu2 != null) {
         if(taulu1.length > 0 && taulu2[0].length > 0) {
            // Alustetaan taulukko esiintymille.
            int[] frekvenssit = new int[taulu1.length];
            // Silmukoidaan taulukot läpi ja lasketaan esiintymät.
            for(int i = 0; i< taulu2.length; i++) {
               for(int j = 0; j < taulu2[0].length; j++) {
                  for(int k = 0; k < taulu1.length; k++) {
                     if(taulu2[i][j] == taulu1[k]) {
                        frekvenssit[k] = frekvenssit[k]+1;
                     }
                  }
               }
            }
            return frekvenssit;
         }
         else
          return null;
      }
      else
         return null;
   }
   public static void main (String[]args) {
      // Alustetaan taulukot.
      char[] merkit1 = {'+','-'};
      
      char[][] merkit11 = {{'+','|','+','|'}, {'-','-','-','-'}, {'|','+','-','|'}};
      
      // Kutsutaan frekvenssit laskevaa metodia ja annetaan taulukot 
      // paramtreiksi ja sijoitetaan paluuarvo muuttujaan.
      int[] tulos = laskeFrekvenssit(merkit1, merkit11);
      
      // Alustetaan taulukot.
      char[] merkit2 = {'a','b'};
      
      char[][] merkit22 = {{'a','b'}, {'b','a'}};
      
      // Kutsutaan frekvenssit laskevaa metodia ja annetaan taulukot 
      // paramtreiksi ja sijoitetaan paluuarvo muuttujaan.
      tulos = laskeFrekvenssit(merkit2, merkit22);
      
      // Alustetaan taulukot.
      char[] merkit3 = null;
      
      char[][] merkit33 = null;
      
      // Kutsutaan frekvenssit laskevaa metodia ja annetaan taulukot 
      // paramtreiksi ja sijoitetaan paluuarvo muuttujaan.
      tulos = laskeFrekvenssit(merkit3, merkit33);
   }
}