// Otetaan Scanner-luokka käyttöön import-lauseella, joka on ohjelman
// alussa ennen luokan määrittelyä.
import java.util.Scanner;

/*
 * Viikkoharjoitus 5, tehtävä 1.
 *
 * Lausekielinen ohjelmointi II, syksy 2020.
 *
 * Ohjelma päättelee käyttäjältä saadun merkkijonon lyhimmän osan pituuden.
 *
 * Esteri Jokinen (esteri.jokinen@tuni.fi).
 *
 */
 
public class ShortestPart {
   /*
    * Metodi päättelee parametrinaan saadun merkkijonon
    * lyhimmän osan ja palauttaa sen pituuden.
    * Jos merkkijono on null-avoinen tai tyhjä,
    * paluuarvo on -1.
    */
   public static int mittaaLyhinOsa(String mj) {
      // Tarkastetaan parametrin oikeellisuus.
      if(mj != null && mj != "") {
         // Pilkotaan merkkijonon osat taulukoksi
         String[] osat = mj.split("[ ]");
         
         // Apumuttuja lyhimmän osan pituudelle.
         int lyhin = osat[0].length();
         
         // Tutkitaan taulukkoa ja päätellään mikä on lyhin osa.
         for(int i = 1; i < osat.length; i++) {
            if (osat[i].length() < lyhin) {
               lyhin = osat[i].length();
            }
         }
         return lyhin;
      }
      else
         return -1;
   }
   public static void main (String[] args) {
      // Tervehditään ja kerrotaan mitä tehdään.
      System.out.println("Hello! I find the length of the shortest substring.");
      
      // Esitellään viite (muuttuja), luodaan syötevirtaa lukeva
      // olio ja liitetään viite olioon.
      Scanner lukija = new Scanner(System.in);
      
      // Pyydetään merkkijono
      System.out.println("Please, enter a string:");
      String merkkijono = lukija.nextLine();
      
      // Kutsutaan lyhimmän osan päättelevää metodia.
      // Annetaan merkkijono parametriksi ja sijoitetaan paluuarvo muuttujaan.
      int tulos = mittaaLyhinOsa(merkkijono);
      
      // Tarkastellaan tulosta ja tulostetaan se jos 
      // parametrin on ollut oikeellinen.
      if (tulos > -1) {
         System.out.println("The length is " + tulos + ".");
      }
   }
}