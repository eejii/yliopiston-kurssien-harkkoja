/*
 * Viikkoharjoitus 5, tehtävä 5.
 *
 * Lausekielinen ohjelmointi II, syksy 2020.
 *
 * Ohjelma vaihtaa 2D taulukon alkioden paikat ja tulostaa taulukon.
 *
 * Esteri Jokinen (esteri.jokinen@tuni.fi).
 *
 */
 
public class CharacterSwitcher {
   /*
    * Metodi saa parametrina taulukon ja kaksi merkkiä ja vaihtaa merkkien
    * paikkoja taulukossa. Jos taulukolle on varattu muistia paluuarvo on true, 
    * muuten se on false.
    */
   public static boolean vaihdaMerkit (char[][] taulu, char m1, char m2) {
      // Tarkastetaan, että taulukolle on varattu muistia.
      if (taulu != null && taulu[0].length > 0) {
         // Käydään taulukko läpi silmukoiden avulla ja
         // vaihdetaan merkit.
         for(int i = 0; i < taulu.length; i++) {
            for (int j = 0; j < taulu[0].length; j++) {
               if(taulu[i][j] == m1) {
                  taulu[i][j] = m2;
               }
               else if(taulu[i][j] == m2) {
                     taulu[i][j] = m1;
                  }
            }
         }
         return true;
      }
      else 
         return false;
   }
   /*
    * Metodi tulostaa parametrina saadun taulukon alkiot,
    * eikä palauta mitään.
    */
   public static void tulosta2d (char[][] taulu) {
      // Tarkastetaan, että taulukolle on varattu muistia.
      if(taulu != null) {
         // Tulostetaan alkiot silmukoiden avulla.
         for(int i = 0; i < taulu.length; i++) {
            for(int j = 0; j < taulu[0].length; j++) {
               System.out.print(taulu[i][j]);
            }
            System.out.println();
         }
      }
   }
   
   public static void main (String[]args) {
      // ALustetaan ja luodaan taulukot.
      char[][] taulukko = {{'a','<', 'b'}, {'a', '>', 'b'}};
      
      // Kutsutaan merkkejä vaihtavaa metodia ja annetaan parametrina
      // taulukko ja vaihdettavat merkit. Sijoitetaan paluuarvo muuttujaan.
      boolean tulos = vaihdaMerkit(taulukko, 'a', 'b');
      
      // Tulostetaan mikäli taulukolle on varattu muistia.
      if(tulos == true){
         tulosta2d(taulukko);
      }
      
      char[][] taulukko1 = null;
      
      tulos = vaihdaMerkit(taulukko1, 'a', 'b');
      
      if(tulos == true){
         tulosta2d(taulukko1);
      }
      
      char[][] taulukko2 = {{'+','-','+'}, {'+','-','+'}, {'+','-','+'}};
      
      tulos = vaihdaMerkit(taulukko2, '+', '-');
      
      if(tulos == true){
         tulosta2d(taulukko2);
      }
      
      char[][] taulukko3 = {{}};
      
      tulos = vaihdaMerkit(taulukko3, 'u', 'i');
      
      tulosta2d(taulukko3);
   }
}