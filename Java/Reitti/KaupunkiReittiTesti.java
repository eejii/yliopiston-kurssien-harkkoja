/* 
 * Viikkoharjoitus 7, teht�v� 5
 *
 * Kaupunkia ja reitti� testaava luokka.
 *
 * Esteri Jokinen, Jokinen.Esteri.E@student.uta.fi.
 *
 */
 
 public class KaupunkiReittiTesti {
    public static void main(String[] args) {
       // Luodaan kaupunkeja
       Kaupunki tampere = new Kaupunki("Tampere");
       Kaupunki jamsa = new Kaupunki("J�ms�");
       Kaupunki jyvaskyla = new Kaupunki("Jyv�skyl�");
    
       // Tulostetaan kaupunkien tiedot.
       System.out.println(tampere);
       System.out.println(jamsa);
       System.out.println(jyvaskyla);
       
       // Tehd��n uusi reitti
       Reitti matka = new Reitti(170);
       matka.lisaa(tampere);
       matka.lisaa(jamsa);
       matka.lisaa(jyvaskyla);
       
       // Tulostetaan reitti
       System.out.println(matka);
    }
 }