import java.util.LinkedList;
/*
 * Viikkoharjoitus 7, teht�v� 5
 *
 * Kaupunkien kautta kulkevaa reitti� mallintava luokka.
 *
 * Esteri Jokinen, esteri.jokinen@tuni.fi.
 *
 */

public class Reitti {
   // Reitin pituus (km)
   private int pituus;
   
   // Kokoelma reitin kaupungeille.
   LinkedList<Kaupunki> reitti;
   
   public Reitti(int uusiPituus) {
      pituus(uusiPituus);
      
      // Luodaan uusi kokoelmaolio ja liitet��n attribuutti siihen.
      reitti = new LinkedList<Kaupunki>();
   }
   
   public int pituus() {
      return pituus;
   }
   
   public void pituus(int uusiPituus) {
      if (uusiPituus >= 0)
         pituus = uusiPituus;
   }
   
   public void lisaa(Kaupunki uusi) {
      if (uusi instanceof Kaupunki)
         reitti.add(uusi);
   }
   
   @Override
   public String toString() {
      // Ker�t��n kaupungit merkkijonoon
      String merkkijonona = "";
      for (Kaupunki kaupunki : reitti)
         merkkijonona = merkkijonona + kaupunki + " ";
      
      // Lis�t��n pituus
      merkkijonona = pituus + " km : " + merkkijonona;
      
      // Palautetaan muodostettu esitys.
      return merkkijonona;
   }
}