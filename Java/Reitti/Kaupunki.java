/* 
 * Viikkoharjoitus 7, teht�v� 5
 *
 * Kaupunkia mallintava luokka.
 * 
 * Esteri Jokinen, esteri.jokinen@tuni.fi.
 *
 */
 
 public class Kaupunki {
    // Kaupungin nimi
    private String nimi;
    
    public Kaupunki(String uusiNimi) {
       nimi(uusiNimi);
    }
    
    public String nimi() {
       return nimi;
    }
    
    public void nimi(String uusiNimi) {
       if (uusiNimi != null && uusiNimi.length() > 0)
          nimi = uusiNimi;
    }
    
    @Override
    public String toString() {
       return nimi;
    }
 }